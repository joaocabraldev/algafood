alter table forma_pagamento add data_atualizacao datetime null;
update forma_pagamento set data_atualizacao = curdate();
alter table forma_pagamento alter column data_atualizacao set not null;