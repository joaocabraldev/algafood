function consultarRestaurantes() {
    $.ajax({
        url: 'http://localhost:8080/restaurantes',
        type: 'get',
        success: function(response) {
            $('#content').text(JSON.stringify(response))
        },
        error: function(error) {
            alert('Erro ao consultar Dados')
        }
    })
}

$('#action').click(consultarRestaurantes)
